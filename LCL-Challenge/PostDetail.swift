//
//  PostDetail.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-21.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import Foundation

struct PostDetail: Codable {
    let selftext: String
    let title: String
    let thumbnail_height: Int?
    let thumbnail_width: Int?
    let thumbnail: String?
    
    var image: Data?
    
    private enum CodingKeys: String, CodingKey {
        case selftext
        case title
        case thumbnail_height
        case thumbnail_width
        case thumbnail
        case image
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        selftext = try container.decode(String.self, forKey: .selftext)
        title = try container.decode(String.self, forKey: .title)
        thumbnail_height = try? container.decode(Int.self, forKey: .thumbnail_height)
        thumbnail_width = try? container.decode(Int.self, forKey: .thumbnail_width)
        thumbnail = try? container.decode(String.self, forKey: .thumbnail)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(selftext, forKey: .selftext)
        try container.encode(title, forKey: .title)
        try container.encode(thumbnail_height, forKey: .thumbnail_height)
        try container.encode(thumbnail_width, forKey: .thumbnail_width)
        try container.encode(thumbnail, forKey: .thumbnail)
        try container.encode(image, forKey: .image)
    }
}

struct Posts: Codable {
    let data: PostDetail
}

struct RedditPost: Codable {
    struct RedditData: Codable {
        let children: [Posts]?
    }
    let data: RedditData?
}


