//
//  PostDetailViewController.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-20.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {
    var post: PostDetail!
    
    @IBOutlet weak var detailPostLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailPostLabel.text = post.selftext.isEmpty ? "No Text Found" : post.selftext
    }
}

