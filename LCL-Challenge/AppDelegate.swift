//
//  AppDelegate.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-20.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

