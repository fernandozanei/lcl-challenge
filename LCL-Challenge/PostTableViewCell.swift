//
//  PostTableViewCell.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-21.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
