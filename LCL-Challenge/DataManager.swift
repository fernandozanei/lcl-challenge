//
//  DataManager.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-20.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import Foundation

class DataManager {
    
    func loadData(completion: @escaping (_ posts: [PostDetail]) -> Void) {
        guard let url = URL(string: "https://www.reddit.com/r/swift.json") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return
            }
            do {
                let decoder = JSONDecoder()
                let jsonResponse = try decoder.decode(RedditPost.self, from: dataResponse)
                
                guard let postArray = jsonResponse.data?.children else {
                    return
                }
                
                var posts = postArray.map{ $0.data }
                
                posts = posts.map({ (post) -> PostDetail in
                    var multablePost = post
                    if let imageUrl = post.thumbnail {
                      if imageUrl.contains("http") {
                        multablePost.image = try? Data(contentsOf: URL(string: imageUrl)!)
                        }
                    }
                    return multablePost
                })
                
                

                completion(posts)
                
            } catch { print(error) }
        }
        task.resume()
    }
    
}

