//
//  PostTableViewController.swift
//  LCL-Challenge
//
//  Created by Fernando Zanei on 2018-11-21.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import UIKit

class PostTableViewController: UITableViewController {
    
    var posts = [PostDetail]()
    var thumbnailImages: [Int: UIImage] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.accessibilityIdentifier = "PostTableView"
        let data = DataManager()
        data.loadData { (dataPosts) in
            DispatchQueue.main.async {
                self.posts = dataPosts
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        
        let post = posts[indexPath.row]
        
        cell.titleLabel.text = post.title
        cell.postImage.image = thumbnailImages[indexPath.row]

        if (cell.postImage.image == nil && post.image != nil) {
            let image = UIImage(data: post.image!)
            thumbnailImages[indexPath.row] = image
            cell.postImage.image = image
        }
        

        cell.postImage.removeConstraints(cell.postImage.constraints)

        if let width = post.thumbnail_width {
            cell.postImage.translatesAutoresizingMaskIntoConstraints = false
            cell.postImage.widthAnchor.constraint(equalToConstant: CGFloat(width)).isActive = true
        }
        
        
        tableView.reloadRows(at: [indexPath], with: .none)

        
        return cell
    }
    
    
     // MARK: - Navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? PostDetailViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                destinationVC.navigationItem.title = posts[indexPath.row].title
                destinationVC.post = posts[indexPath.row]
            }
        }
     }
    
}
