//
//  LCL_ChallengeTests.swift
//  LCL-ChallengeTests
//
//  Created by Fernando Zanei on 2018-11-20.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import XCTest
@testable import LCL_Challenge

class LCL_ChallengeTests: XCTestCase {

    var viewControllerUnderTest: PostTableViewController!

    let dataManager = DataManager()
    var posts = [PostDetail]()


    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.viewControllerUnderTest = storyboard.instantiateViewController(withIdentifier: "PostTableViewController") as? PostTableViewController
        
        // in view controller, menuItems = ["one", "two", "three"]
        self.viewControllerUnderTest.loadView()
        self.viewControllerUnderTest.viewDidLoad()
        
        let asyncDone = expectation(description: "Async function")
        dataManager.loadData(completion: {response in
            DispatchQueue.main.async {
                self.viewControllerUnderTest.posts = response
                self.viewControllerUnderTest.tableView.reloadData()
            }
            asyncDone.fulfill()
        })
        wait(for: [asyncDone], timeout: 10)
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testResponseAPI() {        
        XCTAssertFalse(viewControllerUnderTest.posts.isEmpty)
    }

    func testHasATableView() {
        XCTAssertNotNil(viewControllerUnderTest.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(viewControllerUnderTest.tableView.delegate)
    }
    
//    func testTableViewConfromsToTableViewDelegateProtocol() {
//        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDelegate.self))
//        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:didSelectRowAt:))))
//    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(viewControllerUnderTest.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.numberOfSections(in:))))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cell = viewControllerUnderTest.tableView(viewControllerUnderTest.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? PostTableViewCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "postCell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }
    
    func hasSegue_ForTransitioningTo_ViewConrollerOne() {
        
        let identifiers = segues(ofViewController: viewControllerUnderTest)
        XCTAssertEqual(identifiers.count, 1, "Segue count should equal two.")
        
    }
    
    // Mark - Function to get segues
    
    func segues(ofViewController viewController: UIViewController) -> [String] {
        let identifiers = (viewControllerUnderTest.value(forKey: "storyboardSegueTemplates") as? [AnyObject])?.compactMap({ $0.value(forKey: "identifier") as? String }) ?? []
        return identifiers
    }
}
