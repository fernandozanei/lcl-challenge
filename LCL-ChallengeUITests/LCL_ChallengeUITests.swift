//
//  LCL_ChallengeUITests.swift
//  LCL-ChallengeUITests
//
//  Created by Fernando Zanei on 2018-11-20.
//  Copyright © 2018 fernandozanei. All rights reserved.
//

import XCTest
@testable import LCL_Challenge

class LCL_ChallengeUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false
        
        app = XCUIApplication()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        
        app.launch()
        
        let articleTableView = app.tables["PostTableView"]
        XCTAssertTrue(articleTableView.exists, "The article tableview exists")
        let tableCells = articleTableView.cells
        
        if tableCells.count > 0 {
            // how to
        }
        
    }
    
}
